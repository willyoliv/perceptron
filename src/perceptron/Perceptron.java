package perceptron;

public class Perceptron {
    //matriz com as entradas
    public double[][] matriz_entrada = {{-0.6508,0.1097,4.0009},
                          {-1.4492,0.8896,4.4005},
                          {2.0850,0.6876,12.0710},
                          {0.2626,1.1476,7.7985},
                          {0.6418,1.0234,7.0427},
                          {0.2569,0.6730,8.3265},
                          {1.1155,0.6043,7.4446},
                          {0.0914,0.3399,7.0677},
                          {0.0121,0.5256,4.6316},
                          {-0.0429,0.4660,5.4323},
                          {0.4340,0.6870,8.2287},
                          {0.2735,1.0287,7.1934},
                          {0.4839,0.4851,7.4850},
                          {0.4089,-0.1267,5.5019},
                          {1.4391,0.1614,8.5843},
                          {-0.9115,-0.1973,2.1962},
                          {0.3654,1.0475,7.4858},
                          {0.2144,0.7515,7.1699},
                          {0.2013,1.0014,6.5489},
                          {0.6483,0.2183,5.8991},
                          {-0.1147,0.2242,7.2435},
                          {-0.7970,0.8795,3.8762},
                          {-1.0625,0.6366,2.4707},
                          {0.5307,0.1285,5.6883},
                          {-1.2200,0.7777,1.7252},
                          {0.3957,0.1076,5.6623},
                          {-0.1013,0.5989,7.1812},
                          {2.4482,0.9455,11.2095},
                          {2.0149,0.6192,10.9263},
                          {0.2012,0.2611,5.4631}
                          }; 
    public double[][] amostras = {{-0.3665, 0.0620, 5.9891},
                                  {-0.7842, 1.1267, 5.5912},
                                  {0.3012, 0.5611, 5.8234},
                                  {0.7757, 1.0648,8.0677},
                                  {0.1570, 0.8028, 6.3040},
                                  {-0.7014, 1.0316, 3.6005},
                                  {0.3748, 0.1536, 6.1537},
                                  {-0.6920, 0.9404, 4.4058},
                                  {-1.3970, 0.7141, 4.9263},
                                  {-1.8842, -0.2805, 1.2548}
                                  };
    public String[] Y = new String[10];
    public double[] pesos = new double[4]; //vetor com os pesos
    public double[] pesos_inicial = new double[4];
    public double[] saidas = new double[30]; //vetor com as saidas
    public double[] saidas_esperadas = {-1,-1,-1,1,1,-1,1,-1,1,1,-1,1,-1,-1,-1,-1,1,1,1,1,-1,1,1,1,1,-1,-1,1,-1,1}; //saidas esperadas
    public double N = 0.01;
    public int epoca = 0; 
    public boolean erro = true;
    public double u = 0;
    
    public Perceptron( ){    
        gerar_matriz_pesos();
    }
    public final void gerar_matriz_pesos(){
        for(int i = 0; i < 4 ; i++){
            pesos[i] = (double) (Math.random());
            pesos_inicial[i] = pesos[i];
        }
    }
    public void fase_operacao(){
        for(int i = 0; i < amostras.length; i++){
                    u = (amostras[i][0] * pesos[0]) + (amostras[i][1] * pesos[1]) + 
                            (amostras[i][2] * pesos[2]) + pesos[3];
                if(u >= 0){
                    Y[i] = "P2";
                }else{
                    Y[i] = "P1";
                }
        }
    }
    public void treinamento(){
        while(this.erro){
            //System.out.println(erro);
            erro = false;
            for(int i = 0; i < matriz_entrada.length; i++){
                    u = (matriz_entrada[i][0] * pesos[0]) + (matriz_entrada[i][1] * pesos[1]) + 
                            (matriz_entrada[i][2] * pesos[2]) + pesos[3];
                    //System.out.println(u);
                if(u >= 0){
                    saidas[i] = 1;
                }else{
                    saidas[i] = -1;
                }
                
                if(saidas[i] != saidas_esperadas[i]){
                    //System.out.println(saidas[i] + "   " + saidas_esperadas[i]);
                    for(int j = 0; j < 3; j++){
                        pesos[j] = pesos[j] + (N*( -saidas[i] + saidas_esperadas[i])*matriz_entrada[i][j]);
                    }
                    pesos[3] = pesos[3] +( N *(-saidas[i] + saidas_esperadas[i]));
                    erro = true;
                }
               
            }
            epoca++;
        }
        
    }
}
